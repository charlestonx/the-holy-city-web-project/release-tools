# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::AutoDeploy::Tag do
  subject(:task) { described_class.new }

  around do |ex|
    ClimateControl.modify(AUTO_DEPLOY_BRANCH: '42-1-auto-deploy-2021010200') do
      in_coordinator_pipeline do
        ex.run
      end
    end
  end

  def stub_passing_build(commit_id)
    passing_build = instance_double(
      ReleaseTools::PassingBuild,
      execute: double('Commit', id: commit_id)
    )

    allow(ReleaseTools::PassingBuild).to receive(:new)
      .and_return(passing_build)
  end

  def stub_deploy_version(ref: '14.0.202105311735+ac7ff9aa2f2.2ab4d081326')
    product_version = build(
      :product_version,
      releases: build(
        :releases_metadata,
        omnibus_gitlab_ee: build(:component_metadata, ref: ref)
      )
    )

    allow(ReleaseTools::ProductVersion)
      .to receive(:last_auto_deploy)
      .and_return(product_version)
  end

  after do
    FileUtils.rm_r('deploy_vars.env') if File.exist?('deploy_vars.env')
  end

  describe '#execute' do
    it 'performs a full tagging run' do
      expect(task).to receive(:find_passing_build)

      expect(task).to receive(:build_omnibus).and_return(false)
      expect(task).to receive(:build_cng).and_return(true)
      expect(task).to receive(:tag_coordinator)

      expect(task).to receive(:store_deploy_version)
      expect(task).to receive(:sentry_release)
      expect(task).to receive(:upload_metadata)

      expect(task).to receive(:notify)

      task.execute
    end

    it 'does not tag coordinator without builder changes' do
      expect(task).to receive(:find_passing_build)

      expect(task).to receive(:build_omnibus).and_return(false)
      expect(task).to receive(:build_cng).and_return(false)
      expect(task).not_to receive(:tag_coordinator)

      expect(task).to receive(:store_deploy_version)
      expect(task).to receive(:sentry_release)
      expect(task).to receive(:upload_metadata)

      expect(task).to receive(:notify)

      task.execute
    end

    context 'without a coordinator pipeline' do
      it 'does not execute specific tasks' do
        allow(ReleaseTools::AutoDeploy).to receive(:coordinator_pipeline?)
          .and_return(false)

        expect(task).to receive(:find_passing_build)

        expect(task).to receive(:build_omnibus).and_return(false)
        expect(task).to receive(:build_cng).and_return(true)
        expect(task).to receive(:tag_coordinator)

        expect(task).not_to receive(:store_deploy_version)
        expect(task).not_to receive(:sentry_release)
        expect(task).not_to receive(:upload_metadata)

        expect(task).not_to receive(:notify)

        task.execute
      end
    end
  end

  describe '#find_passing_build' do
    it 'finds a passing build' do
      passing_build = stub_const('ReleaseTools::PassingBuild', spy)

      task.find_passing_build

      expect(passing_build).to have_received(:new).with(task.branch.to_s)
      expect(passing_build).to have_received(:execute)
    end
  end

  describe '#build_omnibus' do
    it 'executes Omnibus builder' do
      stub_passing_build('b81056529d1f')
      builder = stub_const('ReleaseTools::AutoDeploy::Builder::Omnibus', spy)
      metadata = double('metadata')
      allow(task).to receive(:metadata).and_return(metadata)

      task.build_omnibus

      expect(builder).to have_received(:new)
        .with(task.branch, 'b81056529d1f', metadata)
      expect(builder).to have_received(:execute)
    end
  end

  describe '#build_cng' do
    it 'executes CNG builder' do
      stub_passing_build('79def06ea756')
      builder = stub_const('ReleaseTools::AutoDeploy::Builder::CNGImage', spy)
      metadata = double('metadata')
      allow(task).to receive(:metadata).and_return(metadata)

      task.build_cng

      expect(builder).to have_received(:new)
        .with(task.branch, '79def06ea756', metadata)
      expect(builder).to have_received(:execute)
    end
  end

  describe '#tag_coordinator' do
    it 'executes Coordinator tagger' do
      tagger = stub_const('ReleaseTools::AutoDeploy::Tagger::Coordinator', spy)

      task.tag_coordinator

      expect(tagger).to have_received(:new)
      expect(tagger).to have_received(:tag!)
    end
  end

  describe '#sentry_release' do
    it 'tracks a Sentry release' do
      stub_passing_build('cdec0111a65a')
      tracker = stub_const('ReleaseTools::Deployments::SentryTracker', spy)

      task.sentry_release

      expect(tracker).to have_received(:new)
      expect(tracker).to have_received(:release).with('cdec0111a65a')
    end
  end

  describe '#upload_metadata' do
    it 'uploads release metadata in a coordinator pipeline' do
      uploader = stub_const('ReleaseTools::ReleaseMetadataUploader', spy)

      expect(ReleaseTools::AutoDeploy::Tag).to receive(:current)
        .and_return('1.2.3')

      metadata = double('metadata')
      allow(task).to receive(:metadata).and_return(metadata)

      task.upload_metadata

      expect(uploader).to have_received(:new)
      expect(uploader).to have_received(:upload)
        .with('1.2.3', metadata, auto_deploy: true)
    end
  end

  describe '#prepopulate_metadata' do
    it 'fetches the latest metadata and returns a pre-populated instance' do
      product_version = build(:product_version)

      expect(ReleaseTools::ProductVersion)
        .to receive(:last_auto_deploy)
        .and_return(product_version)

      instance = described_class.new
      meta = instance.metadata

      expect(meta).to be_instance_of(ReleaseTools::ReleaseMetadata)
      expect(meta.releases.size).to eq(product_version.metadata['releases'].size)
      meta.releases.each do |component, release|
        expect(release.version).to eq(product_version[component].version)
        expect(release.sha).to eq(product_version[component].sha)
        expect(release.ref).to eq(product_version[component].ref)
        expect(release.tag).to be(product_version[component].tag)
      end
    end

    it 'allows prepopulated metadata to be empty' do
      expect(ReleaseTools::ProductVersion)
        .to receive(:last_auto_deploy)
        .and_return(build(:product_version, version: '42.1.1', releases: {}))

      instance = described_class.new
      meta = instance.metadata

      expect(meta).to be_instance_of(ReleaseTools::ReleaseMetadata)
      expect(meta.releases).to be_empty
    end
  end

  describe '#store_deploy_version' do
    it 'stores deploy_version in an env variable' do
      stub_deploy_version

      without_dry_run do
        task.store_deploy_version
      end

      expect(File.read('deploy_vars.env')).to eq('DEPLOY_VERSION=14.0.202105311735-ac7ff9aa2f2.2ab4d081326')
    end

    it 'formats deploy packages' do
      stub_deploy_version

      result = 'DEPLOY_VERSION=14.0.202105311735-ac7ff9aa2f2.2ab4d081326'

      without_dry_run do
        task.store_deploy_version
      end

      expect(File.read('deploy_vars.env')).to eq(result)
    end

    it 'does nothing in dry-run mode' do
      stub_deploy_version

      task.store_deploy_version

      expect(File).not_to exist('deploy_vars.env')
    end
  end
end
