package pipelines

import "github.com/xanzy/go-gitlab"

type pipelinesClient interface {
	ListProjectPipelines(interface{}, *gitlab.ListProjectPipelinesOptions, ...gitlab.RequestOptionFunc) ([]*gitlab.PipelineInfo, *gitlab.Response, error)
}

func newClient(token string) (pipelinesClient, error) {
	gitlab, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://ops.gitlab.net/api/v4"))
	if err != nil {
		return nil, err
	}

	return gitlab.Pipelines, nil
}
