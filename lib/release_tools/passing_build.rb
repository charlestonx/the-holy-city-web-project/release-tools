# frozen_string_literal: true

module ReleaseTools
  class PassingBuild
    include ::SemanticLogger::Loggable

    attr_reader :ref

    def initialize(ref, project = ReleaseTools::Project::GitlabEe)
      @project = project
      @ref = ref
    end

    def execute
      passing_build_commit
    end

    def next_commit
      commits.next_commit(passing_build_commit.id)
    end

    private

    def commits
      @commits ||= ReleaseTools::Commits.new(@project, ref: ref)
    end

    def passing_build_commit
      @passing_build_commit ||= begin
        commit =
          if Feature.enabled?(:auto_deploy_tag_latest)
            commits.latest
          else
            commits.latest_successful_on_build(since_last_auto_deploy: true)
          end

        if commit.nil?
          raise "Unable to find a passing #{@project} build for `#{ref}` on dev"
        end

        commit
      end
    end
  end
end
