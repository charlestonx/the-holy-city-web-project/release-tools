# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class UnreleasedCommits
      include ::SemanticLogger::Loggable

      MINIMUM_LENGTH_MERGE_COMMIT = 2

      # version - A ReleaseTools::Version instance
      # project - A ReleaseTools::Project using Managed Versioning
      def initialize(version, project)
        @version = version
        @project = project
      end

      def execute
        return [] unless version.valid?

        commits = Retriable.with_context(:api) do
          comparison.fetch('commits', []).select do |commit|
            commit.parent_ids.length >= MINIMUM_LENGTH_MERGE_COMMIT
          end
        end

        logger.info('Fetching unreleased commits',
                    project: project,
                    tag: last_tag,
                    branch: stable_branch,
                    commits: commits.count)

        commits
      rescue Gitlab::Error::NotFound
        logger.warn('Unreleased commits were not found',
                    project: project,
                    tag: last_tag,
                    branch: stable_branch)
        []
      end

      def total_pressure
        execute.count
      end

      private

      attr_reader :version, :project

      def client
        ReleaseTools::GitlabClient
      end

      def comparison
        client
          .compare(project, from: last_tag, to: stable_branch)
      end

      def last_tag
        project.tag_for(version)
      end

      def stable_branch
        project.stable_branch_for(version)
      end
    end
  end
end
