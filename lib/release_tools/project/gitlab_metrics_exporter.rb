# frozen_string_literal: true

module ReleaseTools
  module Project
    class GitlabMetricsExporter < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/gitlab-metrics-exporter.git',
        dev: 'git@dev.gitlab.org:gitlab/gitlab-metrics-exporter.git',
        security: 'git@gitlab.com:gitlab-org/security/gitlab-metrics-exporter.git'
      }.freeze

      def self.version_file
        'GITLAB_METRICS_EXPORTER_VERSION'
      end

      def self.default_branch
        'main'
      end

      def self.metadata_project_name
        'gitlab_metrics_exporter'
      end
    end
  end
end
